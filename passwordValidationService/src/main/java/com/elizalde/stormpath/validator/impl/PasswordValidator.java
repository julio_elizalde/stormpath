package com.elizalde.stormpath.validator.impl;

import java.util.regex.Pattern;

import com.elizalde.stormpath.validator.IPasswordValidator;
import com.elizalde.stormpath.validator.ValidationRules;

public class PasswordValidator implements IPasswordValidator {

	public boolean validateWithRegEx(String password) {

		return validateAllRegExRules(password);

	}

	public boolean validateSingleRule(String regex, String password) {
		return validateRegExRule(regex, password);
	}

	// in here we can add extra validations that might be very difficult to do
	// only with Regular Expressions
	public boolean validate(String password) {
		
		return true;
	}

	public boolean validateAllRegExRules(String password) {
		// first check for null or empty
		if (!isNullorEmpty(password)) {

			// validate all rules in the ValidationRules enum
			for (ValidationRules rule : ValidationRules.values()) {
				//only if rule is enabled will be checked
				if (rule.isEnabled()) {
					//if any rule is not passed will return false
					if (!validateRegExRule(rule.getRuleRegEx(), password)) {
						System.err.println(rule.getRuleDesc());
						return false;
					}
				}

			}

		} else {
			System.err.println("Password cannot be null or empty.");
			return false;
		}

		return true;
	}

	public boolean isNullorEmpty(String password) {
		// return true if password is null or empty
		if (null == password || password.isEmpty())
			return true;
		return false;
	}

	public boolean validateRegExRule(String regex, String password) {

		if (Pattern.matches(regex, password)) {
			return true;
		}
		return false;
	}

}
