package com.elizalde.stormpath.validator;



public enum ValidationRules {

	//in this enum you can add as many rules as you need
	LENGHT_1   (1, ".{5,12}", true,                 "Must be between 5 and 12 characters in length."),
	LOWERCASE_AND_DIGITS_2 (2, "^(?=.*[a-z])(?=.*\\d).+$",true, "Must consist of a mixture of lowercase letters" +
														        " and numerical digits only, with at least one of each."),
	SAME_SEQUENCE_3        (3, "(?!.*(.{2,})\\1).*", true,  "Must not contain any sequence of characters immediately followed by the same sequence.");

	
		
	
	private ValidationRules(int ruleNum, String ruleRegEx,boolean enabled, String ruleDesc) {
		this.ruleNum = ruleNum;
		this.ruleRegEx = ruleRegEx;
		this.enabled = enabled;
		this.ruleDesc = ruleDesc;
		
	}
	
	private int ruleNum;
	private String ruleRegEx;
	private String ruleDesc;
	private boolean enabled;
	
	
	public String getRuleRegEx() {
		return ruleRegEx;
	}
	public void setRuleRegEx(String ruleRegEx) {
		this.ruleRegEx = ruleRegEx;
	}
	
	public String getRuleDesc() {
		return ruleDesc;
	}
	public void setRuleDesc(String ruleDesc) {
		this.ruleDesc = ruleDesc;
	}

	
	

		

	/**
	 * This static method returns the rule description for a rule num
	 * 
	 * @param ruleNum
	 * @return
	 */
	public static String getRuleDesc(int ruleNum) {

		String ruleDesc = "";
		ValidationRules[] rules = ValidationRules.values();

		for (ValidationRules rule : rules) {
			if (rule != null && rule.ruleNum == ruleNum) {
				ruleDesc = rule.ruleDesc;
				System.out.println("Rule description found");
				break;
			}
		}
		if (ruleDesc.equals("")) {
			System.err.println("Rule description not found");

		}
		return ruleDesc;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}


	
	

}
