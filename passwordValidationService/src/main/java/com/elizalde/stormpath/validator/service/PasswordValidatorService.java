package com.elizalde.stormpath.validator.service;

import com.elizalde.stormpath.validator.IPasswordValidator;

public class PasswordValidatorService {

	IPasswordValidator passwordValidator;

	// this method will validate all available rules
	public boolean validate(String password) {
		// first, all validation rules implemented using Reg Ex
		boolean r = passwordValidator.validateWithRegEx(password);
		// second, any extra validation not implemented using Reg Ex
		boolean r2 = passwordValidator.validate(password);

		if (r && r2)
			return true;
		else
			return false;

	}

	// this one is for testing purpuses to test the rules one by one
	public boolean validateOne(String regEx, String password) {
		return passwordValidator.validateSingleRule(regEx, password);
	}

	public void setPasswordValidator(IPasswordValidator passwordValidator) {
		this.passwordValidator = passwordValidator;
	}

}
