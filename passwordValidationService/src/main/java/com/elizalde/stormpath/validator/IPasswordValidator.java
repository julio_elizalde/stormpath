package com.elizalde.stormpath.validator;

public interface IPasswordValidator {
	//validates using regex
	public boolean validateWithRegEx(String password);
	//for testing individual regex rules
	public boolean validateSingleRule(String regEx, String password);
	//this method is for have extra validations not necessary using regex
	public boolean validate(String password);


}
