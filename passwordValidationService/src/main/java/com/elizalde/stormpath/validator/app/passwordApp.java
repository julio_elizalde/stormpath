package com.elizalde.stormpath.validator.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.elizalde.stormpath.validator.service.PasswordValidatorService;

public class passwordApp {

	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "spring-config.xml" });

		PasswordValidatorService service = (PasswordValidatorService) context.getBean("PasswordValidatorService");
			
		 String[] goodPasswords = {"stormpath21", "1a2b3c4d", "1234567890jc", "1234a","stormk1storm"};
		 
		 String[] badPasswords = {"", "1", "5sp", "123567890", "stormpath", "Stormpath", "StormPath13", "12stormstorm"};
		
		    for (String good : goodPasswords) {
		        boolean valid = service.validate(good);
		        if (valid) {
		            System.out.println("Your Password " + good + " is valid!");
		        }
		    }

		    for (String bad : badPasswords) {
		    	 boolean valid = service.validate(bad);
			        if (!valid) {
			            System.err.println("Your Password " + bad + " is not valid!\n");
			        }
		    }
	}

}
