package com.elizalde.stormpath.validator.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.elizalde.stormpath.validator.ValidationRules;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring-config-test.xml" })
public class PasswordValidatorServiceTest {
	@Autowired
	PasswordValidatorService pvs;
	
	@Test
	public void atLeastOneNumberTest() {
		String regEx = ValidationRules.LOWERCASE_AND_DIGITS_2.getRuleRegEx();

		
		assertEquals(true, pvs.validateOne(regEx ,"1passwd"));
		assertEquals(true, pvs.validateOne(regEx ,"strange4all"));
		assertEquals(true, pvs.validateOne(regEx ,"a1"));
		assertEquals(true, pvs.validateOne(regEx ,"1d"));
		assertEquals(false, pvs.validateOne(regEx ,"nonumber"));
		
	}
	
	@Test
	public void atLeastOneLowercaseLetterTest() {
		String regEx = ValidationRules.LOWERCASE_AND_DIGITS_2.getRuleRegEx();

		assertEquals(true, pvs.validateOne(regEx ,"b14234") );
		assertEquals(true, pvs.validateOne(regEx ,"2343c123") );
		assertEquals(true, pvs.validateOne(regEx ,"12323j") );
		assertEquals(false, pvs.validateOne(regEx ,"a") );
		assertEquals(false, pvs.validateOne(regEx ,"1") );
		assertEquals(false, pvs.validateOne(regEx ,"#@)($*#@()$") );
		assertEquals(false, pvs.validateOne(regEx ,"3732474738") );

	}
	
	@Test
	public void numberLowercaseOnlyTest() {
		String regEx = ValidationRules.LOWERCASE_AND_DIGITS_2.getRuleRegEx();

		assertEquals(true,pvs.validateOne(regEx ,"ab2342"));
		assertEquals(true, pvs.validateOne(regEx ,"a1"));
		assertEquals(false, pvs.validateOne(regEx ,"123WHATLONGPASS"));
		assertEquals(false, pvs.validateOne(regEx ,"@#$#$"));
		assertEquals(false, pvs.validateOne(regEx ,"ASDSsss"));
	}
	
	
	@Test
	public void lengthRuleTest() {
		String regEx = ValidationRules.LENGHT_1.getRuleRegEx();

		assertEquals(true,  pvs.validateOne(regEx ,"qwert"));//5
		assertEquals(true,  pvs.validateOne(regEx ,"123456789qwr"));//12
		assertEquals(false, pvs.validateOne(regEx ,"1a"));
		assertEquals(false, pvs.validateOne(regEx ,"123456789qwr1"));//13
		assertEquals(false, pvs.validateOne(regEx ,"somelongpasswordthatIwillforget"));
	}
	
	@Test
	public void sameSequenceRuleTest() {
		String regEx = ValidationRules.SAME_SEQUENCE_3.getRuleRegEx();
		assertEquals(true, pvs.validateOne(regEx , "qwertygffghfqwerty"));
		assertEquals(true, pvs.validateOne(regEx , "123nsjjs123"));
		assertEquals(true, pvs.validateOne(regEx , "juliocesarjulio"));
		assertEquals(true, pvs.validateOne(regEx , "onetwoone"));
		
		assertEquals(false, pvs.validateOne(regEx , "strongpswpsw"));
		assertEquals(false, pvs.validateOne(regEx , "123abcabc"));
		assertEquals(false, pvs.validateOne(regEx , "somedif_samesequencesamesequence"));

		assertEquals(false, pvs.validateOne(regEx , "juliojulio"));
		assertEquals(false, pvs.validateOne(regEx , "5151"));
		
		
	}

}
